package com.service.order.controller;

import com.service.order.common.PageableCustom;
import com.service.order.common.ResponseFactory;
import com.service.order.domain.dto.ProductDto;
import com.service.order.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/products")
@Tag(name = "Products", description = "API module products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @PostMapping()
    @Transactional(rollbackOn = Exception.class)
    @Operation(
            summary = "Tao mơí sản phẩm",
            description = "Tao mơí sản phẩm")
    public ResponseEntity<?> create(@RequestBody ProductDto request)    {
        productService.create(request);
        return ResponseFactory.success();
    }

    @PutMapping()
    @Transactional(rollbackOn = Exception.class)
    @Operation(
            summary = "Update sản phẩm",
            description = "Update sản phẩm")
    public ResponseEntity<?> update(@RequestBody ProductDto request)    {
        productService.update(request);
        return ResponseFactory.success();
    }

    @GetMapping("")
    @Operation(
            summary = "Lấy thông tin  sản phẩm theo id",
            description = "Lấy thông tin  sản phẩm theo id")
    public ResponseEntity<?> get(@RequestParam("id") Long id)    {
        return ResponseFactory.success(productService.detail(id));
    }

    @DeleteMapping("/{id}")
    @Transactional(rollbackOn = Exception.class)
    @Operation(
            summary = "Xóa sản phẩm theo id",
            description = "Xóa sản phẩm theo id")
    public ResponseEntity<?> delete(@PathVariable("id") Long id)  {
        productService.delete(id);
        return ResponseFactory.success();
    }

    @GetMapping("/search")
    @Operation(
            summary = "Tìm kiếm thông tin  sản phẩm ",
            description = "Tìm kiếm thông tin  sản phẩm")
    public ResponseEntity<?> search(@RequestParam(value = "name", required = false) String name, @RequestParam(value = "description" ,required = false) String description) throws IOException {
        return ResponseFactory.success(productService.search(name, description));
    }

    @GetMapping("/all")
    @Operation(
            summary = "Lấy tất cả thông tin  sản phẩm ",
            description = "Tất cả thông tin  sản phẩm")
    public ResponseEntity<?> all(@RequestParam(value = "page", required = false, defaultValue = "1") int page, @RequestParam(value = "pageSize" ,required = false, defaultValue = "5") int pageSize) throws IOException {
        PageableCustom response = productService.getAll(page, pageSize);
        return ResponseFactory.success(response);
    }

}
