package com.service.order.controller;

import com.service.order.common.ResponseFactory;
import com.service.order.domain.dto.OrderDetailDto;
import com.service.order.domain.dto.ProductDto;
import com.service.order.service.OrderService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/orders")
@Tag(name = "Order", description = "API module order")
public class OrderController {

    @Autowired
    OrderService orderService;

    @PostMapping()
    @Transactional(rollbackOn = Exception.class)
    @Operation(
            summary = "Tạo đơn hàng",
            description = "Tạo đơn hàng mới")
    public ResponseEntity<?> create(@RequestBody @Valid OrderDetailDto request)    {
        orderService.create(request);
        return ResponseFactory.success();
    }

    @PutMapping()
    @Transactional(rollbackOn = Exception.class)
    @Operation(
            summary = "Update đơn hàng",
            description = "Update đơn hàng mới")
    public ResponseEntity<?> update(@RequestBody @Valid OrderDetailDto request)    {
        orderService.update(request);
        return ResponseFactory.success();
    }   

    @GetMapping("")
    @Operation(
            summary = "Lay thong tin đơn hàng theo id",
            description = "Lay thong tin đơn hàng theo id")
    public ResponseEntity<?> get(@RequestParam(value = "id", required = false) Long id, @RequestParam(value = "status", required = false) Integer status)    {
        return ResponseFactory.success(orderService.get(id, status));
    }

    @DeleteMapping("/{id}")
    @Transactional(rollbackOn = Exception.class)
    @Operation(
            summary = "Xoa đơn hàng theo id",
            description = "Xoa đơn hàng theo id")
    public ResponseEntity<?> delete(@PathVariable("id") Long id)    {
        orderService.delete(id);
        return ResponseFactory.success();
    }

    @GetMapping("/search")
    @Operation(
            summary = "Tìm kiếm đơn hang theo ten hoac id",
            description = "Tìm kiếm đơn hang theo ten hoac id")
    public ResponseEntity<?> search(@RequestParam(value = "id", required = false) Long id,
                                    @RequestParam(value = "name", required = false) String name,
                                    @RequestParam(value = "status", required = false) Integer status) throws IOException {
        return ResponseFactory.success(orderService.search(name, id, status));
    }
}
