package com.service.order.repository.elasticsearch;

import com.service.order.domain.es.ESOrderEntity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ESOrderRepository extends ElasticsearchRepository<ESOrderEntity, Long> {
}
