package com.service.order.repository.elasticsearch;

import com.service.order.domain.es.ESProductEntity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ESProductRepository extends ElasticsearchRepository<ESProductEntity, Long> {
}
