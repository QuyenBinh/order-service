package com.service.order.service.mapper;

import com.service.order.domain.OrderEntity;
import com.service.order.domain.es.ESOrderEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface OrderMapper {

    ESOrderEntity convertEntityToES(OrderEntity order);
}
