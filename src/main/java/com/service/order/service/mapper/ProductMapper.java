package com.service.order.service.mapper;

import com.service.order.domain.ProductEntity;
import com.service.order.domain.dto.ProductDto;
import com.service.order.domain.es.ESProductEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductMapper {

    ProductDto convertProductEntityToProductDto(ProductEntity product);

    ProductEntity convertDtoToProduct(ProductDto dto);

    ESProductEntity convertDtoToProductES(ProductEntity dto);

    ProductDto convertESProductEntityToProductDto(ESProductEntity product);

    List<ProductDto> convertESProductEntityToProductDto(List<ESProductEntity> products);

    List<ProductDto> convertListProductEntityToListProductDto(List<ProductEntity> product);
}
