package com.service.order.service;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch._types.query_dsl.QueryBuilders;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.search.Hit;
import com.service.order.common.BusinessLogicException;
import com.service.order.controller.response.OrderDetailResponse;
import com.service.order.controller.response.ProductOrder;
import com.service.order.domain.OrderDetail;
import com.service.order.domain.OrderEntity;
import com.service.order.domain.ProductEntity;
import com.service.order.domain.dto.OrderDetailDto;
import com.service.order.domain.dto.ProductOrders;
import com.service.order.domain.es.ESOrderEntity;
import com.service.order.repository.OrderDetailRepository;
import com.service.order.repository.OrderRepository;
import com.service.order.repository.ProductRepository;
import com.service.order.repository.elasticsearch.ESOrderRepository;
import com.service.order.repository.elasticsearch.ESProductRepository;
import com.service.order.service.mapper.OrderMapper;
import com.service.order.service.mapper.ProductMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class OrderService {

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private OrderDetailRepository orderDetailRepository;
    @Autowired
    private ESOrderRepository esOrderRepository;
    @Autowired
    private ESProductRepository esProductRepository;
    @Autowired
    ProductMapper productMapper;
    @Autowired
    OrderMapper orderMapper;
    @Autowired
    ElasticsearchClient elasticsearchClient;

    public void create(OrderDetailDto request) {
        List<Long> idsProducts = request.getProductOrders().stream().map(ProductOrders::getId).toList();
        Map<Long, Long> map = request.getProductOrders().stream().collect(Collectors.toMap(ProductOrders::getId, ProductOrders::getQuantity));
        List<ProductEntity> productEntities = productRepository.findAllById(idsProducts);
        AtomicReference<Double> price = new AtomicReference<>(0.0);
        List<ProductEntity> products = new ArrayList<>();
        map.forEach((k, v) -> {
            ProductEntity productEntity = productEntities.stream().filter(e -> Objects.equals(e.getId(), k)).findFirst().orElse(null);
            if (!Objects.isNull(productEntity)) {
                if(productEntity.getQuantity() < v) {
                    throw new BusinessLogicException("Số lượng đặt hàng lớn hơn số lượng trong kho!!!!");
                }
                if (productEntity.getQuantity() == 0L) {
                    throw new BusinessLogicException(String.format("Sản phẩm %s đã hết. Vui lòng chọn sản phẩm khác", productEntity.getName()));
                }
                Double priceTotal = price.get();
                price.set(priceTotal + productEntity.getPrice() * v);
                // update số lượng product còn lại
                productEntity.setQuantity(productEntity.getQuantity() - v);
                products.add(productEntity);
            }
        });
        OrderEntity order = OrderEntity.builder()
                .customerName(request.getCustomerName())
                .address(request.getCustomerAddress())
                .email(request.getCustomerEmail())
                .phone(request.getCustomerPhoneNumber())
                .status(1)
                .price(price.get())
                .build();
        OrderEntity orderSave = orderRepository.save(order);
        esOrderRepository.save(orderMapper.convertEntityToES(orderSave));
        List<OrderDetail> orderDetails = new ArrayList<>();
        request.getProductOrders().forEach(item -> {
            OrderDetail orderDetail = OrderDetail.builder()
                    .orderId(orderSave.getId())
                    .productId(item.getId())
                    .quantity(item.getQuantity())
                    .build();
            orderDetails.add(orderDetail);
        });
        orderDetailRepository.saveAll(orderDetails);
        productRepository.saveAll(products);
    }

    public void update(OrderDetailDto request) {
        OrderEntity order = orderRepository.findById(request.getOrderId()).orElse(null);
        if (Objects.isNull(order)) {
            throw new BusinessLogicException("Không tìm thấy thông tin đơn hàng!!!");
        }
        if (!Objects.isNull(request.getCustomerAddress())) {
            order.setAddress(request.getCustomerAddress());
        }
        if (!Objects.isNull(request.getCustomerName())) {
            order.setCustomerName(request.getCustomerName());
        }
        if (!Objects.isNull(request.getCustomerEmail())) {
            order.setEmail(request.getCustomerEmail());
        }
        if (!Objects.isNull(request.getCustomerPhoneNumber())) {
            order.setPhone(request.getCustomerPhoneNumber());
        }
        List<OrderDetail> orderDetailList = new ArrayList<>();
        List<ProductEntity> products = new ArrayList<>();
        if (!Objects.isNull(request.getProductOrders())) {
            List<OrderDetail> orderDetails = orderDetailRepository.findAllByOrderId(request.getOrderId());
            List<Long> idsProducts = request.getProductOrders().stream().map(ProductOrders::getId).toList();
            Map<Long, Long> map = request.getProductOrders().stream().collect(Collectors.toMap(ProductOrders::getId, ProductOrders::getQuantity));
            List<ProductEntity> productEntities = productRepository.findAllById(idsProducts);
            AtomicReference<Double> price = new AtomicReference<>(order.getPrice());
            map.forEach((k, v) -> {
                OrderDetail or = orderDetails.stream().filter(e -> Objects.equals(e.getProductId(), k)).findAny().orElse(null);
                if(!Objects.isNull(or)) {
                    ProductEntity productEntity = productEntities.stream().filter(e -> Objects.equals(e.getId(), or.getProductId())).findFirst().orElse(null);
                    if (!Objects.isNull(productEntity)) {
                        if(productEntity.getQuantity() < v) {
                            throw new BusinessLogicException("Số lượng đặt hàng lớn hơn số lượng trong kho!!!!");
                        }
                        if (productEntity.getQuantity() == 0L) {
                            throw new BusinessLogicException(String.format("Sản phẩm %s đã hết. Vui lòng chọn sản phẩm khác", productEntity.getName()));
                        }
                        Double priceTotal = price.get();
                        if(or.getQuantity() < v)    {
                            priceTotal += productEntity.getPrice()*(v - or.getQuantity() );
                        } else {
                            priceTotal -= productEntity.getPrice()*(or.getQuantity() - v);
                        }
                        price.set(priceTotal);
                    }
                    // update số lượng product còn lại
                    productEntity.setQuantity(productEntity.getQuantity() + or.getQuantity() - v);
                    products.add(productEntity);
                    or.setQuantity(v);
                    orderDetailList.add(or);
                }
            });
            order.setPrice(price.get());
        }
        order.setStatus(request.getStatus());
        orderRepository.save(order);
        esOrderRepository.save(orderMapper.convertEntityToES(order));
        orderDetailRepository.saveAll(orderDetailList);
        productRepository.saveAll(products);
    }

    public void delete(Long id) {
        OrderEntity order = orderRepository.findById(id).orElse(null);
        if(Objects.isNull(order))   {
            throw new BusinessLogicException("Không tìm thấy thông tin đơn hàng!!!");
        }
        orderRepository.deleteById(order.getId());
        esOrderRepository.deleteById(order.getId());
        orderDetailRepository.deleteAllByOrderId(id);
    }

    public OrderDetailResponse get(Long id, Integer status) {
        OrderEntity order = orderRepository.findByIdAndStatus(id, status);
        if(Objects.isNull(order))   {
            throw new BusinessLogicException("Không tìm thấy thông tin đơn hàng!!!");
        }
        List<OrderDetail> orderDetails = orderDetailRepository.findAllByOrderId(order.getId());
        Map<Long, OrderDetail> orderDetailMap = orderDetails.stream().collect(Collectors.toMap(OrderDetail::getProductId, Function.identity()));
        List<Long> productIds = orderDetails.stream().map(OrderDetail::getProductId).toList();
        Map<Long, ProductEntity> productsMap = productRepository.findAllById(productIds).stream().collect(Collectors.toMap(ProductEntity::getId, Function.identity()));
        List<ProductOrder> productOrders = new ArrayList<>();
        orderDetailMap.forEach((k,v) -> {
            ProductEntity productEntity = productsMap.get(k);
            if(!Objects.isNull(productEntity))   {
                ProductOrder productOrder = new ProductOrder();
                OrderDetail orderDetail = orderDetailMap.get(k);
                productOrder.setQuantity(orderDetail.getQuantity());
                productOrder.setProducts(productMapper.convertProductEntityToProductDto(productEntity));
                productOrders.add(productOrder);
            }
        });
        OrderDetailResponse response = OrderDetailResponse.builder()
                .orderId(id)
                .customerAddress(order.getAddress())
                .customerName(order.getCustomerName())
                .customerPhoneNumber(order.getPhone())
                .priceTotal(order.getPrice())
                .customerEmail(order.getEmail())
                .products(productOrders)
                .status(order.getStatus())
                .build();
        return response;
    }

    public List<OrderDetailResponse> search(String name, Long id, Integer status) throws IOException {
        if(!Objects.isNull(id)) {
            return List.of(get(id, status));
        }
        String searchText = null;
        if(!Objects.isNull(name))   {
            searchText = name;
        }
        String finalSearchText = searchText;
        Query query = Query.of(q -> q.match(t -> t .field("customerName")
                .query(finalSearchText)));
        SearchResponse<ESOrderEntity> response = elasticsearchClient.search(s -> s
                        .index("orders")
                        .query(query),
                ESOrderEntity.class
        );
        List<ESOrderEntity> esProductEntities = response.hits().hits().stream().map(Hit::source).toList();
        List<OrderDetailResponse> orderDetailResponses = new ArrayList<>();
        esProductEntities.forEach(item -> {
            orderDetailResponses.add(get(item.getId(), status));
        });
        return orderDetailResponses;
    }


}
