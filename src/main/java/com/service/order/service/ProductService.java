package com.service.order.service;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.search.Hit;
import com.service.order.common.BusinessLogicException;
import com.service.order.common.PageableCustom;
import com.service.order.service.mapper.ProductMapper;
import com.service.order.domain.ProductEntity;
import com.service.order.domain.dto.ProductDto;
import com.service.order.domain.es.ESProductEntity;
import com.service.order.repository.ProductRepository;
import com.service.order.repository.elasticsearch.ESProductRepository;
import jakarta.validation.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;


@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ESProductRepository esProductRepository;
    @Autowired
    ProductMapper productMapper;
    @Autowired
    ElasticsearchClient elasticsearchClient;

    public void create(ProductDto request) {
        ProductEntity product = productMapper.convertDtoToProduct(request);
        ProductEntity save = productRepository.save(product);
        esProductRepository.save(productMapper.convertDtoToProductES(save));
    }

    public void update(ProductDto request) {
        ProductEntity productEntity = productRepository.findById(request.getId()).orElse(null);
        if (Objects.isNull(productEntity)) {
            throw new BusinessLogicException("Không tìm thấy sản phẩm");
        }
        ProductEntity product = productMapper.convertDtoToProduct(request);
        ProductEntity save = productRepository.save(product);
            esProductRepository.save(productMapper.convertDtoToProductES(save));
    }

    public ProductDto detail(Long id) {
        ProductEntity productEntity = productRepository.findById(id).orElse(null);
        if (Objects.isNull(productEntity)) {
            throw new BusinessLogicException("Không tìm thấy sản phẩm");
        }
        return productMapper.convertProductEntityToProductDto(productEntity);
    }

    public void delete(Long id) {
        ProductEntity productEntity = productRepository.findById(id).orElse(null);
        if (Objects.isNull(productEntity)) {
            throw new BusinessLogicException("Không tìm thấy sản phẩm");
        }
        productRepository.delete(productEntity);
        esProductRepository.deleteById(productEntity.getId());
    }

    public List<ProductDto> search(String name, String description) throws IOException {

        String searchText = null;
        if(!Objects.isNull(name))   {
            searchText = name;
        }
        if(!Objects.isNull(description))   {
            searchText = description;
        }
        String finalSearchText = searchText;
        SearchResponse<ESProductEntity> response = elasticsearchClient.search(s -> s
                .index("products")
                .query(q -> q
                    .match(t -> t
                        .field("name")
                        .query(finalSearchText)
                    )
                ),
            ESProductEntity.class
        );
        List<ESProductEntity> esProductEntities = response.hits().hits().stream().map(Hit::source).toList();

        return productMapper.convertESProductEntityToProductDto(esProductEntities);
    }

    public PageableCustom getAll(int page, int pageSize)  {
        List<ProductEntity> productEntities = productRepository.findAll();
        List<ProductEntity> data = productEntities.stream()
                .sorted(Comparator.comparing(ProductEntity::getId))
                .skip((long) (page - 1) * pageSize)
                .limit(pageSize)
                .toList();
        return new PageableCustom(productEntities.size(), page, pageSize, data);
    }



}
