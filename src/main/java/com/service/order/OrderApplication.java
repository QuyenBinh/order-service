package com.service.order;

import com.service.order.common.GlobalExceptionHandler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@SpringBootApplication
@EnableElasticsearchRepositories(basePackages = "com.service.order.repository.elasticsearch")
public class OrderApplication {

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(OrderApplication.class);
		app.run(args);
	}

}
