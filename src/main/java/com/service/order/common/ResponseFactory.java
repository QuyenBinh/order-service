package com.service.order.common;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.util.Date;

public class ResponseFactory {
    public static ResponseEntity<Object> success(Object obj) {
        BaseResponse response = new BaseResponse();
        response.setMessage("success");
        response.setStatus(HttpStatus.OK.value());
        response.setDate(new Date());
        response.setData(obj);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    public static ResponseEntity<Object> success() {
        BaseResponse response = new BaseResponse();
        response.setMessage("success");
        response.setStatus(HttpStatus.OK.value());
        response.setDate(new Date());
        response.setData(null);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
