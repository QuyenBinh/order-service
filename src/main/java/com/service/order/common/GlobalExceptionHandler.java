package com.service.order.common;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {


    @ExceptionHandler(value = BusinessLogicException.class)
    public ExceptionResponse handlerBusinessException(BusinessLogicException  ex)   {
        ExceptionResponse exceptionResponse = new ExceptionResponse(HttpStatus.BAD_REQUEST.value(), ex.getMessage());
        return exceptionResponse;
    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public Map<String, String>  handlerMethodArgumentNotValidException(MethodArgumentNotValidException  ex)   {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getFieldErrors().forEach(item -> {
            errors.put(item.getField(), item.getDefaultMessage());
        });
        return errors;
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handlerException(Exception ex, WebRequest webRequest)   {
        ExceptionResponse exceptionResponse = new ExceptionResponse(HttpStatus.BAD_REQUEST.value(), "Hệ thống gặp sự cố, vui lòng thử lại sau!!!");
        return new ResponseEntity<>(exceptionResponse, HttpStatus.BAD_REQUEST);
    }
}
