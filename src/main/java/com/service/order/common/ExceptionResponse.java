package com.service.order.common;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class ExceptionResponse {

    public ExceptionResponse(int code, String mes)  {
        this.code = code;
        this.message = mes;
    }

    private int code;
    private String message;
}
