package com.service.order.domain.es;

import jakarta.persistence.Column;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.WriteTypeHint;

import java.io.Serializable;
import java.util.Date;

@Document(indexName = "orders", writeTypeHint = WriteTypeHint.FALSE)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ESOrderEntity implements Serializable {

    @Id
    private Long id;
    @Field(type = FieldType.Date)
    private Date createAt;
    @Field(type = FieldType.Text)
    private String customerName;
    @Field(type = FieldType.Text)
    private String address;
    @Field(type = FieldType.Text)
    private String email;
    @Field(type = FieldType.Text)
    private String phone;
    @Field(type = FieldType.Long)
    // status = 0 => hủy, = 1 => đã đặt hàng...
    private Integer status;
    @Field(type = FieldType.Double)
    private Double price;

}
