package com.service.order.domain.es;


import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.WriteTypeHint;

import java.io.Serializable;

@Document(indexName = "products", writeTypeHint = WriteTypeHint.FALSE)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ESProductEntity implements Serializable {

    @Id
    private Long id;
    @Field(type = FieldType.Text)
    private String name;
    @Field(type = FieldType.Text)
    private String description;
    @Field(type = FieldType.Double)
    private Double price;
    @Field(type = FieldType.Double)
    private Long quantity;
}
