package com.service.order.domain.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class OrderDetailDto {

    @Schema(description = "id đơn hàng")
    private Long orderId;
    @Schema(description = "Tên khách hàng")
    private String customerName;
    @Schema(description = "Địa chỉ khách hàng")
    private String customerAddress;
    @Schema(description = "Email khách hàng")
    @Email(message = "Email không hợp lệ")
    private String customerEmail;
    @Schema(description = "Số điện thoại khách hàng")
    @Pattern(regexp = "^\\d{10}$", message = "Số điện thoại không hợp lệ")
    private String customerPhoneNumber;
    @Schema(description = "Sản phẩm đặt hàng")
    private List<ProductOrders> productOrders;
    @Schema(description = "trạng thái đơn hàng 1 => đã đặt, 0 => hủy....")
    private Integer status;

}
