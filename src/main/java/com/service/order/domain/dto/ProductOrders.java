package com.service.order.domain.dto;

import lombok.Data;

@Data
public class ProductOrders {

    private Long id;
    private Long quantity;
}
