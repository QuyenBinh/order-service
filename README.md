# order-service

1. Thiết kế database
- Service gồm 3 bảng như hình

![Alt text](image.png)

- Thiết kế bảng order-details
+ Với mỗi đơn hàng -> có thể có nhiều sản phẩm -> mỗi sản phẩm có thể có nhiều số lượng 
+ bảng này sẽ lưu trữ thông tin chi tiết về đơn hàng gồm:
    - Id đơn hàng (order_id)
    - id sản phẩm (product_id)
    - Số lượng mỗi sản phẩm của đơn hàng đó (quantity)
2. Cách chạy project
- clone souce code
- cd vào foler chứa project
- chạy file service.sh để run mysql và elasticsearch
- chạy project với active profiles : dev
- link swagger: http://localhost:8081/order-service/swagger-ui/index.html#/
3. Tối ưu và đánh index
- Đánh index trường status bảng order
- Đánh index trường order_id bảng order_details
- các API liên quan đến tìm kiếm theo tên, mô tả của product vả order em sử dụng
elasticseach để tìm kiếm.