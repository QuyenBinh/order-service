#!/usr/bin/env bash

DOCKER_SERVICE_PATH=$PWD/src/main/java/com/service/order/docker
echo ${DOCKER_SERVICE_PATH}
cd ${DOCKER_SERVICE_PATH}

docker-compose -f services.yml up -d
